# Outer request

_All_ messages from the client are as follows:

```
{
	id : client id encrypted with server public key
	time : time of sending
	request : inner JSON request
	hash : hash of id+time+request encrypted with the client's private key and then the session key
}
```

The time and request fields are encrypted with:

* The server public key if this is the initial handshake message, or
* The session key.

# Inner requests

Everything is encrypted with the session key. All requests (except initialisation) have a method name which is used to identify the task desired and may be all that is needed ( e.g. logout, list files).

## Initialise

Sent by client. Recognised by the server if the `id` from the outer request has no existing session.

```
{
	session_key : ...,
	session_iv : ...
}
```

### Server response

The `time` field +1 encrypted with the client's public key and then with the session key.

If the authorisation fails, server responds with HTTP code 403 (Forbidden).

## Upload a file

```
{
	request_method : "upload",
	file_name : encrypted with server's public key,
	contents : encrypted with the client's master key
}
```

## Download a file

```
{
	request_method : "download",
	file_name : encrypted with server's public key
}
```

### Server response

Server may respond with 404: Not Found.

```
{
	contents : ...,
	hash : Hash of contents encrypted with server's private key
}
```

## List files on the server

```
{
	request_method : "list"
}
```

### Server response

```
{
	files : [ file_name1, file_name2, ... ],
	hash : hash of files array encrpyted with server's private key
}
```

## End session

```
{
	request_method : "logout"
}
```

The session expires immediately.

# Server state

A `HashMap<String, Session>` is used to keep track of ongoing sessions.

A `Session` object should contain:

* The client's ID.
* The session key.
* The last interaction time.

A session is expired if the last interaction happened more than `EXPIRY_TIME` (1 hour) ago or if the client ended the session. In the former case the `Session` may be kept for some time after expiry so that a client can be told if a late request is made.

Every request must have a time newer than the most recent time.

# HTTTP codes

All server responses need an appropriate [HTTP code](en.wikipedia.org/wiki/List_of_HTTP_status_codes). In general:

* 200 - OK
* 400 - Bad request : The syntax was incorrect (e.g. invalid JSON or JSON keys not found)
* 408 - Request Timeout : The session key expired.
* 500 - Server error : The server encountered an exception and doesn't know why

# Random ideas

* Random noise in the client ID to prevent any traffic analysis whatsoever.
* Ensuring files are not overwritten.
* Files sent in parts.
* Da checkbox.
* 256 bit encryption
* Better (more efficient) support for large files?
* Delete da files