package gson.request;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import utils.IOUtils;

public class InitialiseRequest implements InnerRequest {

	private String sessionKey;
	private String sessionIV;

	public InitialiseRequest(SecretKey sessionKey, byte[] sessionIV) {
		this.sessionKey = IOUtils.bytesToStringBase64(sessionKey.getEncoded());
		this.sessionIV = IOUtils.bytesToStringBase64(sessionIV);
	}

	public SecretKey getSessionKey() {
		byte[] encodedKey = IOUtils.stringBase64ToBytes(sessionKey);
		SecretKey secretKey = new SecretKeySpec(encodedKey, 0,
				encodedKey.length, "AES");
		return secretKey;

	}

	public byte[] getSessionIV() {
		return IOUtils.stringBase64ToBytes(sessionIV);
	}

}
