package gson.request;

public abstract class ServiceRequest implements InnerRequest {

	protected final String requestMethod;

	protected ServiceRequest(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public String getRequestMethod() {
		return requestMethod;
	}
	
}
