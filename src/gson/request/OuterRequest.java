package gson.request;

public class OuterRequest {

	private final String id;

	private final String request;

	private final String timestamp;

	private final String hash;

	public OuterRequest(String id, String request, String timestampString,
			String hash) {
		this.id = id;
		this.request = request;
		this.timestamp = timestampString;
		this.hash = hash;
	}

	public String getId() {
		return id;
	}

	public String getRequest() {
		return request;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public String getHash() {
		return hash;
	}

}
