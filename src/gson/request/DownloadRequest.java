package gson.request;

public class DownloadRequest extends ServiceRequest {

	// encrypted with private key
	private final String fileName;

	public DownloadRequest(String fileName) {
		super("download");
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

}
