package gson.request;

public class UploadRequest extends ServiceRequest {

	private final String fileName;

	private final String contents;

	public UploadRequest(String fileName, String contents) {
		super("upload");
		this.fileName = fileName;
		this.contents = contents;
	}

	public String getContents() {
		return contents;
	}

	public String getFileName() {
		return fileName;
	}

}
