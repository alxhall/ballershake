package gson.request;

public class LogoutRequest extends ServiceRequest {

	public static final LogoutRequest INSTANCE = new LogoutRequest();
	
	private LogoutRequest() {
		super("logout");
	}

}
