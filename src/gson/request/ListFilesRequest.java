package gson.request;

public class ListFilesRequest extends ServiceRequest {

	public static final ListFilesRequest INSTANCE = new ListFilesRequest();
	
	private ListFilesRequest() {
		super("list");
	}

}
