package gson.response;

public class DownloadResponse {

	private final String contents;

	private final String hash;

	public DownloadResponse(String contents, String hash) {
		this.contents = contents;
		this.hash = hash;
	}

	public String getContents() {
		return contents;
	}

	public String getHash() {
		return hash;
	}

}
