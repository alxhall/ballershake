package gson.response;

import java.util.List;

public class ListFilesResponse {

	private final List<String> filenames;
	
	private final String hash;

	public ListFilesResponse(List<String> filenames, String hash) {
		this.filenames = filenames;
		this.hash = hash;
	}

	public List<String> getFilenames() {
		return filenames;
	}
	
	public String getHash() {
		return hash;
	}

}
