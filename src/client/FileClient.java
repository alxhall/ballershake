package client;

import gson.request.DownloadRequest;
import gson.request.InitialiseRequest;
import gson.request.InnerRequest;
import gson.request.ListFilesRequest;
import gson.request.LogoutRequest;
import gson.request.OuterRequest;
import gson.request.UploadRequest;
import gson.response.DownloadResponse;
import gson.response.ListFilesResponse;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;
import java.util.Scanner;

import javax.crypto.SecretKey;

import server.Session;
import utils.CryptoUtils;
import utils.IOUtils;

import com.google.gson.Gson;

public class FileClient {

	private static final PublicKey SERVER_PUBLIC_KEY = CryptoUtils
			.readPublicKey("res/keys/public_server.key");
	private static final SecretKey LOCAL_MASTER_KEY = CryptoUtils
			.readMasterKey("res/keys/master.key");
	private static final byte[] MASTER_IV = { 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	private static final String DOWNLOADS_DIRECTORY = "downloads/";

	private SecretKey sessionKey = null;
	private byte[] sessionIV = null;
	private String encryptedClientID;
	private long lastTimestamp;

	private final String clientName;
	private final String hostURL;
	private final PrivateKey clientPrivateKey;

	private static final Gson GSON = new Gson();
	
	private static FileClient runningClient;

	public static void main(String[] args) {		
		
		String serverPublicKeyPath = "res/keys/public_server.key";
		if (!IOUtils.checkFileExists(serverPublicKeyPath)) {
			System.err.println("Server public key file not found at: "
					+ serverPublicKeyPath);
			System.exit(0);
		}

		final Scanner scanner = new Scanner(System.in);
		runningClient = null;
		
		try {
			// get Host URL
			String hostURL = "http://localhost:8080";
			System.out
					.println("Please specify host URL: (press enter for default of '"
							+ hostURL + "')");
			String temp = scanner.nextLine();
			if (! (temp.equals("") || temp.equals("\n")) ) {
				hostURL = temp;
			}

			// get username
			String username = "client1";
			System.out
					.println("Please enter your username: (press enter for default of '"
							+ username + "')");
			temp = scanner.nextLine();
			if (! (temp.equals("") || temp.equals("\n")) ) {
				username = temp;
			}

			// check that username has corresponding private key file
			String clientPrivateKeyPath = "res/keys/private_" + username + ".key";
			if (!IOUtils.checkFileExists(clientPrivateKeyPath)) {
				System.err.println("Client private key file not found at: "
						+ clientPrivateKeyPath);
				System.exit(0);
			}

			// create client pointing to specified server
			runningClient = new FileClient(username, hostURL);

			// initialise / login
			if (runningClient.initialise()) {
				System.out.println("Connected!");
				System.out.println();
				
				String[] menu = {
						"*********************************************************************",
						"* Please enter one of the following commands (and parameters):      *",
						"* 'L' : Print list of files stored on the server.                   *",
						"* 'U file_path' : Upload file specified by file_path to server.     *",
						"* 'D file_name' : Download file specified by file_name from server. *",
						"* 'Q' : Logout and terminate the program.                           *",
						"*********************************************************************"};

				while (true) {
					try {

						for (String line : menu) {
							System.out.println(line);
						}

						String input = scanner.nextLine() + " ";
						String choice = input.substring(0, 2).toLowerCase();
						String parameter;

						if (choice.equals("l ")) {
							runningClient.printListFiles();
						} else if (choice.equals("u ")) {
							parameter = input.substring(2, input.length())
									.trim();
							if (IOUtils.checkFileExists(parameter)) {
								runningClient.upload(new File(parameter));
							} else {
								System.err.println("File to upload does not exist: " + parameter);
							}
						} else if (choice.equals("d ")) {
							parameter = input.substring(2, input.length())
									.trim();
							runningClient.download(parameter);
						} else if (choice.equals("q ")) {
							break;
						} else {
							System.err.println("Invalid choice!");
						}
						System.out.println();

					} catch (IndexOutOfBoundsException e) {
						System.err.println("Error processing choice!");
					}
				}

			} else {
				System.err.println("Failed to connect...");
			}

		} finally {
			scanner.close();
			
			if (runningClient != null) {
				runningClient.logout();
				System.out.println("Logged out.");
			}
		}
	}

	public FileClient(String name, String hostURL) {
		this.clientName = name;
		this.hostURL = hostURL;
		this.clientPrivateKey = CryptoUtils
				.readPrivateKey("res/keys/private_" + clientName + ".key");
		encryptedClientID = CryptoUtils.asymmetricEncrypt(SERVER_PUBLIC_KEY,
				name);
	}

	private boolean initialise() {
		sessionKey = CryptoUtils.generateSessionKey();
		sessionIV = CryptoUtils.generateSessionIV();
		InitialiseRequest initRequest = new InitialiseRequest(sessionKey,
				sessionIV);
		String response = sendRequest(initRequest);
		boolean success = false;
		if (response != null) {
			try {

				// Check for match
				long timestamp = Long.parseLong(response);
				success = timestamp == lastTimestamp + 1;
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		return success;
	}

	private String decryptSessionKey(String response) {
		return CryptoUtils.symmetricDecrypt(sessionKey, response, sessionIV);
	}

	private String encryptSessionKey(String string) {
		return CryptoUtils.symmetricEncrypt(sessionKey, string, sessionIV);
	}

	private String encryptServerPublicKey(String string) {
		return CryptoUtils.asymmetricEncrypt(SERVER_PUBLIC_KEY, string);
	}

	private byte[] decryptLocalFile(byte[] encryptedContents) {
		return CryptoUtils.decryptBytesToBytes(encryptedContents, LOCAL_MASTER_KEY, CryptoUtils.SYMMETRIC_ALGORITHM, MASTER_IV);
	}

	private String encryptLocalFile(byte[] contents) {
		return CryptoUtils.encrypt(contents, LOCAL_MASTER_KEY, CryptoUtils.SYMMETRIC_ALGORITHM,	MASTER_IV);
	}

	private OuterRequest createRequest(InnerRequest inner) {
		boolean init = inner instanceof InitialiseRequest;
		String innerRequestString = GSON.toJson(inner);
		
		// Used for demonstrating testing
		System.out.println("Log: Enclosing inner request:\n"+innerRequestString);
		
		lastTimestamp = System.currentTimeMillis();
		String timestampString;
		if (init) {
			timestampString = encryptServerPublicKey(lastTimestamp + "");
			innerRequestString = encryptServerPublicKey(innerRequestString);
		} else {
			timestampString = encryptSessionKey(lastTimestamp + "");
			innerRequestString = encryptSessionKey(innerRequestString);
		}
		String hash = CryptoUtils.hashString(clientName + lastTimestamp
				+ innerRequestString);
		hash = CryptoUtils.asymmetricEncrypt(clientPrivateKey, hash);
		hash = CryptoUtils.symmetricEncrypt(sessionKey, hash, sessionIV);

		return new OuterRequest(encryptedClientID, innerRequestString,
				timestampString, hash);
	}

	private String sendRequest(OuterRequest request) {
		// Make sure that timestamps don't match
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
		}
		try {
			URL url = new URL(hostURL);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			String body = GSON.toJson(request);
			
			// Used for demonstrating testing
			System.out.println("Log: Sending request:\n"+body);

			connection.getOutputStream().write(IOUtils.stringUTFToBytes(body));
			int responseCode = connection.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {

				// Read response
				InputStream inputStream = connection.getInputStream();
				byte[] responseBytes = org.apache.commons.io.IOUtils
						.toByteArray(inputStream);
				
				String response = IOUtils.bytesToStringBase64(responseBytes);
				
				// Used for demonstrating testing
				System.out.println("Log: Receiving response:\n" + response);
				
				response = decryptSessionKey(response);
				
				// Used for demonstrating testing
				System.out.println("Log: Decrypted response:\n" + response);
				return response;
			} else {
				switch (responseCode) {
				case HttpURLConnection.HTTP_BAD_REQUEST:
					System.err.println("The request failed due to bad syntax");
					break;
				case HttpURLConnection.HTTP_NOT_FOUND:
					System.err
							.println("The resource was not found on the server");
					break;
				case HttpURLConnection.HTTP_INTERNAL_ERROR:
					System.err
							.println("An unknown error occurred on the server");
					break;
				case HttpURLConnection.HTTP_CLIENT_TIMEOUT:
					System.err.println("Timeout: the session has expired");
					break;
				default:
					System.err.println("Server returned error code "
							+ responseCode);
				}
				return null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private String sendRequest(InnerRequest inner) {
		return sendRequest(createRequest(inner));
	}

	private void upload(File file) {
		try {
			byte[] fileContents = IOUtils.readBytesFromFile(file);
			String name = file.getName();
			
			if (name.equals(Session.TIMESTAMP_FILENAME)) {
				System.err.println("The filename is reserved");
			} else if (name.length() > 50) {
				System.err.println("Maximum length of file name is 50 characters");
			} else {
				String encryptedName = encryptServerPublicKey(name);
				String encryptedContents = encryptLocalFile(fileContents);
				UploadRequest upload1 = new UploadRequest(encryptedName,
						encryptedContents);
				sendRequest(upload1);
				System.out.println("File successfully uploaded!");
			}
			
		} catch (IOException e) {
			System.err.println("Failed to upload file");
			e.printStackTrace();
		}
	}

	private void download(String filename) {
		DownloadRequest download = new DownloadRequest(
				encryptServerPublicKey(filename));
		String responseBody = sendRequest(download);
		if (responseBody != null) {
			DownloadResponse response = GSON.fromJson(responseBody,
					DownloadResponse.class);
			byte[] byteContents = IOUtils.stringBase64ToBytes(response.getContents());
			if (CryptoUtils.checkHash(response.getHash(), byteContents, SERVER_PUBLIC_KEY)) {

				File downloadPath = new File(DOWNLOADS_DIRECTORY + filename);
				if (downloadPath.getParentFile() != null) {
					downloadPath.getParentFile().mkdirs();
				}
				try {
					downloadPath.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}

				byte[] decryptedContents = decryptLocalFile(byteContents);
				try {
					IOUtils.writeBytesToFile(downloadPath, decryptedContents);
				} catch (IOException e) {
					System.err.println("Failed to decrypt the downloaded file");
					e.printStackTrace();
				}
			} else {
				System.err.println("The hash did not match");
			}
			
			System.out.println("File successfully downloaded!");
		}
	}

	private List<String> listFiles() {
		String responseBody = sendRequest(ListFilesRequest.INSTANCE);
		if (responseBody != null) {
			ListFilesResponse response = GSON.fromJson(responseBody,
					ListFilesResponse.class);

			String hashToCheck = CryptoUtils.hashString(response.getFilenames()
					.toString());
			if (CryptoUtils.asymmetricDecrypt(SERVER_PUBLIC_KEY,
					response.getHash()).equals(hashToCheck)) {
				return response.getFilenames();
			} else {
				System.err.println("Hashes don't match!");
				return null;
			}
		}
		return null;
	}

	private void printListFiles() {
		List<String> list = listFiles();
		if (list == null) {
			System.err.println("Failed to download file list");
		} else if (list.isEmpty()) {
			System.out.println("No files on the server :-(");
		} else {
			System.out.println("Files:\n------");
			for (String name : list) {
				System.out.println(name);
			}
		}
	}

	private void logout() {
		sendRequest(LogoutRequest.INSTANCE);
	}

}
