package server;

import java.io.File;
import java.io.IOException;
import java.security.PublicKey;

import javax.crypto.SecretKey;
import javax.naming.TimeLimitExceededException;

import utils.IOUtils;

public class Session {

	public static final String TIMESTAMP_FILENAME = ".timestamp";

	private static final long EXPIRY_TIME = 60 * 60 * 1000;

	private final PublicKey clientPublicKey;
	private final SecretKey sessionKey;
	private final byte[] sessionIV;
	private long lastInteractionTimeServer;
	private long lastInteractionTimeClient;
	private final File dir;

	private File timestampFile;

	public Session(PublicKey clientPublicKey, SecretKey sessionKey,
			byte[] sessionIV, File dir, long timestamp)
			throws TimeLimitExceededException, IOException {
		this.clientPublicKey = clientPublicKey;
		this.sessionKey = sessionKey;
		this.sessionIV = sessionIV;
		this.dir = dir;
		timestampFile = new File(dir, TIMESTAMP_FILENAME);
		if (!timestampFile.exists()) {
			timestampFile.createNewFile();
		}
		checkNew(timestamp);
		checkNotExpired(timestamp);
		updateLastInteractionTimeServer();
	}

	public SecretKey getSessionKey() {
		return sessionKey;
	}

	public PublicKey getClientPublicKey() {
		return clientPublicKey;
	}

	public byte[] getSessionIV() {
		return sessionIV;
	}

	public File getDirectory() {
		return dir;
	}

	public void checkNew(long timestamp) throws IOException {
		if (lastInteractionTimeClient == 0) {
			String string;
			try {
				string = IOUtils.readStringFromFile(timestampFile).trim();
				if (!string.equals("")) {
					lastInteractionTimeClient = Long.parseLong(string);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (timestamp <= lastInteractionTimeClient) {
			throw new IllegalArgumentException(
					"Message must be newer than most recent message");
		}
		lastInteractionTimeClient = timestamp;
		IOUtils.writeStringToFile(timestampFile, lastInteractionTimeClient + "");
	}

	private void updateLastInteractionTimeServer() {
		lastInteractionTimeServer = System.currentTimeMillis();
	}

	public void checkNotExpired() throws TimeLimitExceededException {
		checkNotExpired(lastInteractionTimeServer);
		updateLastInteractionTimeServer();
	}

	public static void checkNotExpired(long timestamp)
			throws TimeLimitExceededException {
		long now = System.currentTimeMillis();
		checkTimesClose(timestamp, now);
	}

	/**
	 * Check that time1 and time2 are not for apart
	 * 
	 * @throws TimeLimitExceededException
	 */
	private static void checkTimesClose(long time1, long time2)
			throws TimeLimitExceededException {
		boolean result = Math.abs(time2 - time1) < EXPIRY_TIME;
		if (!result) {
			throw new TimeLimitExceededException("Invalid timestamp");
		}
	}

}
