package server;

import gson.request.DownloadRequest;
import gson.request.InitialiseRequest;
import gson.request.ListFilesRequest;
import gson.request.LogoutRequest;
import gson.request.OuterRequest;
import gson.request.ServiceRequest;
import gson.request.UploadRequest;
import gson.response.DownloadResponse;
import gson.response.ListFilesResponse;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.SecretKey;
import javax.naming.TimeLimitExceededException;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.core.Container;
import org.simpleframework.http.core.ContainerServer;
import org.simpleframework.transport.Server;
import org.simpleframework.transport.connect.Connection;
import org.simpleframework.transport.connect.SocketConnection;

import utils.CryptoUtils;
import utils.IOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class FileServer implements Container {

	public static final PrivateKey SERVER_PRIVATE_KEY = CryptoUtils
			.readPrivateKey("res/keys/private_server.key");

	private static final Gson GSON = new GsonBuilder().registerTypeAdapter(
			ServiceRequest.class, new ServiceRequestAdapter()).create();

	Map<String, Session> sessionMap = new HashMap<>();

	public static void main(String[] args) throws IOException {
		Server server = new ContainerServer(new FileServer());
		Connection connection = new SocketConnection(server);
		SocketAddress address = new InetSocketAddress(8080);
		connection.connect(address);
	}

	@Override
	public void handle(Request request, Response response) {
		String clientName = null;
		try {
			String responseContent;
			SecretKey sessionKey;
			byte[] sessionIV;
			byte[] requestBytes = org.apache.commons.io.IOUtils
					.toByteArray(request.getInputStream());
			String requestContent = IOUtils.bytesToStringUTF(requestBytes);

			// Used for demonstrating testing
			System.out.println("Log: Receiving request:\n" + requestContent);

			OuterRequest outerRequest = GSON.fromJson(requestContent,
					OuterRequest.class);
			String id = outerRequest.getId();
			String innerRequestString = outerRequest.getRequest();
			String timestampString = outerRequest.getTimestamp();
			String hash = outerRequest.getHash();

			for (String item : new String[] { id, timestampString, hash }) {
				if (item.length() > 300) {
					throw new IllegalArgumentException("Field too long");
				}
			}

			clientName = decryptPrivateKey(id);
			Session session = sessionMap.get(clientName);

			// If the id is unrecognised, begin new session
			if (session == null) {

				String keyFilename = "res/keys/public_" + clientName + ".key";
				File keyFile = new File(keyFilename);
				if (keyFile.exists()) {
					PublicKey clientPublicKey = CryptoUtils
							.readPublicKey(keyFilename);

					// Read the timestamp on the request
					timestampString = decryptPrivateKey(timestampString);
					long timestamp = Long.parseLong(timestampString);

					String hashSource = clientName + timestamp
							+ innerRequestString;
					innerRequestString = decryptPrivateKey(innerRequestString);

					// Used for demonstrating testing
					System.out.println("Log: Initialise request:\n"
							+ innerRequestString);

					InitialiseRequest initRequest = GSON.fromJson(
							innerRequestString, InitialiseRequest.class);
					sessionKey = initRequest.getSessionKey();
					sessionIV = initRequest.getSessionIV();
					File dir = new File("files/" + clientName + "/");
					if (!dir.exists()) {
						dir.mkdirs();
					}
					session = new Session(clientPublicKey, sessionKey,
							sessionIV, dir, timestamp);
					checkHash(hashSource, hash, session);
					sessionMap.put(clientName, session);
					responseContent = (timestamp + 1) + "";
				} else {
					throw new IllegalAccessException("Unknown client id");
				}
			} else {

				// Existing session

				// Get session encryption details
				sessionKey = session.getSessionKey();
				sessionIV = session.getSessionIV();

				// Check that the session is alive
				session.checkNotExpired();

				// Get timestamp
				timestampString = CryptoUtils.symmetricDecrypt(sessionKey,
						timestampString, sessionIV);
				long timestamp = Long.parseLong(timestampString);

				// Check that this message is more recent than the last
				session.checkNew(timestamp);

				// Check that the hash is correct
				String hashSource = clientName + timestamp + innerRequestString;
				checkHash(hashSource, hash, session);

				// Get the inner service request
				innerRequestString = CryptoUtils.symmetricDecrypt(sessionKey,
						innerRequestString, sessionIV);
				ServiceRequest serviceRequest = GSON.fromJson(
						innerRequestString, ServiceRequest.class);

				// Used for demonstrating testing
				System.out.println("Log: Service request:\n"
						+ innerRequestString);

				// Delegate to the appropriate method
				File dir = session.getDirectory();
				if (serviceRequest instanceof UploadRequest) {
					upload((UploadRequest) serviceRequest, dir);
					responseContent = "";
				} else if (serviceRequest instanceof DownloadRequest) {
					responseContent = download(
							(DownloadRequest) serviceRequest, dir);
				} else if (serviceRequest instanceof ListFilesRequest) {
					responseContent = listFiles(dir);
				} else if (serviceRequest instanceof LogoutRequest) {
					sessionMap.remove(clientName);
					responseContent = "";
				} else {
					throw new IllegalArgumentException(
							"Unknown request method: "
									+ serviceRequest.getRequestMethod());
				}

			}

			// Used for demonstrating testing
			System.out.println("Log: Sending response:\n" + responseContent);

			responseContent = CryptoUtils.symmetricEncrypt(sessionKey,
					responseContent, sessionIV);

			// Used for demonstrating testing
			System.out.println("Log: Sending encrypted response:\n" + responseContent);

			response.getOutputStream().write(
					IOUtils.stringBase64ToBytes(responseContent));
		} catch (TimeLimitExceededException e) {
			sessionMap.remove(clientName);
			response.setCode(HttpURLConnection.HTTP_CLIENT_TIMEOUT);
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			response.setCode(HttpURLConnection.HTTP_NOT_FOUND);
		} catch (JsonParseException | IllegalArgumentException
				| IllegalAccessException e) {
			response.setCode(HttpURLConnection.HTTP_BAD_REQUEST);
			e.printStackTrace();
		} catch (Throwable e) {
			response.setCode(HttpURLConnection.HTTP_INTERNAL_ERROR);
			e.printStackTrace();
		} finally {
			try {
				response.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private String listFiles(File dir) {
		List<String> fileList = new ArrayList<String>();
		for (File child : dir.listFiles()) {
			String name = child.getName();
			if (!name.equals(Session.TIMESTAMP_FILENAME)) {
				fileList.add(name);
			}
		}
		String hash = CryptoUtils.hashString(fileList.toString());
		hash = CryptoUtils.asymmetricEncrypt(SERVER_PRIVATE_KEY, hash);

		ListFilesResponse response = new ListFilesResponse(fileList, hash);

		return GSON.toJson(response);
	}

	private String download(DownloadRequest serviceRequest, File dir)
			throws IOException {
		String filename = CryptoUtils.asymmetricDecrypt(SERVER_PRIVATE_KEY,
				serviceRequest.getFileName());
		File toRead = new File(dir, filename);
		byte[] fileContents = IOUtils.readBytesFromFile(toRead);
		String hashed = CryptoUtils.hashBytes(fileContents);
		hashed = CryptoUtils.asymmetricEncrypt(SERVER_PRIVATE_KEY, hashed);
		DownloadResponse response = new DownloadResponse(
				IOUtils.bytesToStringBase64(fileContents), hashed);
		return GSON.toJson(response);
	}

	private void upload(UploadRequest request, File dir) throws IOException {
		String filename = CryptoUtils.asymmetricDecrypt(SERVER_PRIVATE_KEY,
				request.getFileName());
		File file = new File(dir, filename);
		IOUtils.writeBytesToFile(file,
				IOUtils.stringBase64ToBytes(request.getContents()));
	}

	private String decryptPrivateKey(String string) {
		return CryptoUtils.asymmetricDecrypt(SERVER_PRIVATE_KEY, string);
	}

	private static void checkHash(String hashSource, String hash,
			Session session) {
		hash = CryptoUtils.symmetricDecrypt(session.getSessionKey(), hash,
				session.getSessionIV());
		hash = CryptoUtils
				.asymmetricDecrypt(session.getClientPublicKey(), hash);
		if (!CryptoUtils.checkHash(hash, hashSource)) {
			throw new IllegalArgumentException("Invalid hash");
		}
	}

	private static final class ServiceRequestAdapter implements
			JsonDeserializer<ServiceRequest> {

		private static final Map<String, Class<? extends ServiceRequest>> classMap = new HashMap<String, Class<? extends ServiceRequest>>() {

			private static final long serialVersionUID = 6052840031725306409L;

			{
				put("upload", UploadRequest.class);
				put("download", DownloadRequest.class);
				put("list", ListFilesRequest.class);
				put("logout", LogoutRequest.class);
			}
		};

		@Override
		public ServiceRequest deserialize(JsonElement json, Type typeOfT,
				JsonDeserializationContext context) throws JsonParseException {
			String methodName = json.getAsJsonObject()
					.getAsJsonPrimitive("requestMethod").getAsString();
			Class<? extends ServiceRequest> cls = classMap.get(methodName);
			return (ServiceRequest) context.deserialize(json, cls);
		}
	}

}
