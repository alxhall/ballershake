package utils;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class CryptoUtils {

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	private static final String ASYMMETRIC_ALGORITHM = "RSA/ECB/PKCS1Padding";
	private static final String SYMMETRIC_GENERATOR = "AES";
	public static final String SYMMETRIC_ALGORITHM = "AES/CBC/PKCS5Padding";
	private static final String PROVIDER = "BC";

	public static SecretKey generateSessionKey() {
		KeyGenerator symmetricKeyGen = null;
		try {
			symmetricKeyGen = KeyGenerator.getInstance(SYMMETRIC_GENERATOR,
					PROVIDER);
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			e.printStackTrace();
		}
		symmetricKeyGen.init(128);
		return symmetricKeyGen.generateKey();
	}

	public static byte[] generateSessionIV() {
		return (new SecureRandom()).generateSeed(16);
	}

	public static String symmetricEncrypt(Key key, String string, byte[] iv) {
		return encrypt(string, key, SYMMETRIC_ALGORITHM, iv);
	}

	public static String symmetricDecrypt(Key key, String string, byte[] iv) {
		return decrypt(string, key, SYMMETRIC_ALGORITHM, iv);
	}

	public static String asymmetricEncrypt(Key key, String string) {
		return encrypt(string, key, ASYMMETRIC_ALGORITHM, null);
	}

	public static String asymmetricDecrypt(Key key, String string) {
		return decrypt(string, key, ASYMMETRIC_ALGORITHM, null);
	}

	public static String hashString(String string) {
		MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256", PROVIDER);
			messageDigest.update(IOUtils.stringUTFToBytes(string));
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			e.printStackTrace();
		}
		return IOUtils.bytesToStringBase64(messageDigest.digest());
	}
	
	public static String hashBytes(byte[] bytes) {
		MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256", PROVIDER);
			messageDigest.update(bytes);
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			e.printStackTrace();
		}
		return IOUtils.bytesToStringBase64(messageDigest.digest());
	}

	public static boolean checkHash(String hash, String message) {
		return hash.equals(hashString(message));
	}
	
	public static boolean checkHash(String hash, byte[] message) {
		return hash.equals(hashBytes(message));
	}

	public static boolean checkHash(String hash, String message, Key key) {
		return checkHash(asymmetricDecrypt(key, hash), message);
	}
	
	public static boolean checkHash(String hash, byte[] message, Key key) {
		return checkHash(asymmetricDecrypt(key, hash), message);
	}

	/**
	 * 
	 * @param string a UTF-8 String
	 * @param key
	 * @param algorithm
	 * @param iv
	 * @return a Base64 String
	 */
	public static String encrypt(String string, Key key, String algorithm,
			byte[] iv) {
		return encrypt(IOUtils.stringUTFToBytes(string), key, algorithm, iv);
	}
	
	/**
	 * 
	 * @param string a Base64 String
	 * @param key
	 * @param algorithm
	 * @param iv
	 * @return a UTF-8 String
	 */
	public static String decrypt(String string, Key key, String algorithm,
				byte[] iv) {
		return decrypt(IOUtils.stringBase64ToBytes(string), key, algorithm, iv);
	}
	
	/**
	 * 
	 * @param bytes
	 * @param key
	 * @param algorithm
	 * @param iv
	 * @return a Base64 String
	 */
	public static String encrypt(byte[] bytes, Key key, String algorithm,
			byte[] iv) {
		return IOUtils.bytesToStringBase64(byteCrypt(Cipher.ENCRYPT_MODE, bytes, key, algorithm, iv));
	}
	
	
	/**
	 * 
	 * @param text
	 * @param key
	 * @param algorithm
	 * @param iv
	 * @return a UTF-8 String
	 */
	public static String decrypt(byte[] bytes, Key key, String algorithm,
				byte[] iv) {
		return IOUtils.bytesToStringUTF(byteCrypt(Cipher.DECRYPT_MODE, bytes, key, algorithm, iv));
	}
	
	public static byte[] decryptBytesToBytes(byte[] bytes, Key key, String algorithm,
				byte[] iv) {
		return byteCrypt(Cipher.DECRYPT_MODE, bytes, key, algorithm, iv);
	}
	
	private static byte[] byteCrypt(int cipherMode, byte[] input, Key key, String algorithm,
			byte[] iv) {
		
		byte[] cryptedText = null;
		try {
			final Cipher cipher = Cipher.getInstance(algorithm, PROVIDER);
			if (iv == null) {
				cipher.init(cipherMode, key);
			} else {
				cipher.init(cipherMode, key, new IvParameterSpec(iv));
			}
			cryptedText = cipher.doFinal(input);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return cryptedText;
	}

	public static PublicKey readPublicKey(String path) {
		return ((PublicKey) readKey(path));
	}

	public static PrivateKey readPrivateKey(String path) {
		return ((PrivateKey) readKey(path));
	}

	public static SecretKey readMasterKey(String path) {
		return ((SecretKey) readKey(path));
	}

	public static Key readKey(String path) {
		ObjectInputStream inputStream = null;
		Key key = null;
		try {
			inputStream = new ObjectInputStream(new FileInputStream(path));
			key = (Key) inputStream.readObject();
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return key;
	}

}
