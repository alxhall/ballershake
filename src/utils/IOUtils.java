package utils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;

public class IOUtils {

	public static String bytesToStringBase64(byte[] bytes) {
		return Base64.encodeBase64String(bytes);
	}

	public static String bytesToStringUTF(byte[] bytes) {
		try {
			return new String(bytes, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] stringBase64ToBytes(String string) {
		return Base64.decodeBase64(string);
	}

	public static byte[] stringUTFToBytes(String string) {
		try {
			return string.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean checkFileExists(String filename) {
		File file = new File(filename);
		return (file.exists() && file.isFile());
	}

	public static byte[] readBytesFromFile(File file) throws IOException {
		return org.apache.commons.io.FileUtils.readFileToByteArray(file);
	}

	public static String readStringFromFile(File file) throws IOException {
		return org.apache.commons.io.FileUtils.readFileToString(file);
	}
	
	public static void writeBytesToFile(File file, byte[] content)
			throws IOException {
		org.apache.commons.io.FileUtils.writeByteArrayToFile(file, content);
	}
	
	public static void writeStringToFile(File file, String content) throws IOException {
		org.apache.commons.io.FileUtils.writeStringToFile(file, content);
	}

}
